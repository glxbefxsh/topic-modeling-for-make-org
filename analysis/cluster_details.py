def cluster_details(df):
    corpus_size = df.shape[0]

    # Relatives sizes of the clusters
    n_clusters = df['clustering'].max()
    cluster_sizes = df['clustering'].value_counts().to_dict()
    for clu in cluster_sizes:
        cluster_sizes[clu] /= (corpus_size / 100)
        cluster_sizes[clu] = str(int(cluster_sizes[clu])) + '%'
    return cluster_sizes
