def cluster_words(df, n_words=20):
    # Importing the necessary module
    import numpy as np

    # Finding the number of clusters in the DataFrame
    # by extracting the integer value from the last character of the 'clustering' column
    n_clusters = np.max([int(txt[-1]) for txt in df['clustering'].values]) + 1

    # Creating a dictionary 'word_to_cluster_rp' that will map each word to its frequency in each cluster
    word_to_cluster_rp = {}
    for i in range(df.shape[0]):
        sentence = df['lemmatized'].values[i].split(' ')
        cluster = int(df['clustering'].values[i][-1])
        for word in sentence:
            if word not in word_to_cluster_rp:
                # If word is not present in the dictionary, create a new list of zeros with size 'n_clusters + 1'
                # The last element of the list will store the total frequency of the word
                word_to_cluster_rp[word] = []
                for i in range(n_clusters + 1):
                    word_to_cluster_rp[word].append(0)
            # Increment the count of the word for the current cluster and the total count of the word
            word_to_cluster_rp[word][cluster] += 1
            word_to_cluster_rp[word][-1] += 1

    # Creating a list 'cara_words' that will store the most representative words for each cluster
    cara_words = [[] for i in range(n_clusters)]
    frq = [{} for i in range(n_clusters)]
    for k in range(n_words):
        for clu in range(n_clusters):
            max_score = 0
            # Looping through each word and its frequency in the 'word_to_cluster_rp' dictionary
            for key, value in word_to_cluster_rp.items():
                # Calculating the fraction of the word's frequency in the current cluster to its total frequency
                score = value[clu] * value[clu] / value[-1]
                if score > max_score:
                    # If the calculated fraction is greater than the maximum so far,
                    # set the maximum to the new fraction and the most representative word to the current word
                    max_score = score
                    most_representative_word = key
            # Adding the most representative word to the list of most representative words for the current cluster
            cara_words[clu].append(most_representative_word)
            # Removing the most representative word from the 'word_to_cluster_rp' dictionary
            if most_representative_word in word_to_cluster_rp:
                word_to_cluster_rp.pop(most_representative_word)
            frq[clu][most_representative_word] = max_score

    # Printing the list of most representative words for each cluster
    for clu in range(n_clusters):
        print(f'Cluster {clu}: {cara_words[clu]}')

    from wordcloud import WordCloud

    wordclouds = []

    import matplotlib.pyplot as plt
    plt.gcf().set_facecolor('#ffffff')

    f, axes = plt.subplots(
        1, n_clusters, figsize=(20, 20), facecolor='#ffffff', edgecolor='#ffffff')
    for clu in range(n_clusters):
        wordclouds.append(
            WordCloud(width=800, height=800, background_color='#ffffff').generate_from_frequencies(frq[clu]))
        axes[clu].imshow(wordclouds[clu], interpolation='bilinear')
        axes[clu].axis('off')
        axes[clu].set_title(f'Cluster {clu}', color='#000000')
        axes[clu].patch.set_facecolor('#ffffff')

    return f, axes
