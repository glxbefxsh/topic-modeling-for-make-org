import plotly.express as px


def conditions(ligne, s):
    # si la ligne est dans les 90% plus populaires, qu'elle a un certain écart maximum entre les votes pour et contre
    # puis que les votes neutres ne sont pas majoritaires, on la compte comme controversée
    if (ligne['nbvotes'] > s):
        if (ligne['disagreergb']-ligne['agreergb'])**2 < 50:
            if ligne['neutralrgb'] <= min(ligne['disagreergb'], ligne['agreergb']):
                return 1
    return 0


def controversial_plot(df):
    s = df.max()['nbvotes']/10

    df['controversial'] = df.apply(conditions, args=(s,), axis=1)

    dfnbcontro = df.groupby(['clustering'])['controversial'].sum()

    fig = px.bar(dfnbcontro, x="controversial", barmode="group")

    return (fig)
