import pandas as pd
import json


def transform_dict(d):
    # on garde ce qui nous intéresse pour faire des statistiques à partir du format de base de make.org
    new = {}
    for k, v in d.items():
        if k == 'content':
            new['content'] = v
        # if k == 'author':
        #    for k2, v2 in v.items():
        #        new[k2] = v2
        if k == 'voteKey':
            new[d['voteKey']] = d['count']
            new[d['voteKey']+"rgb"] = d['score']*250
        if k == 'qualificationKey':
            new[d['qualificationKey']] = d['count']

        if isinstance(v, list):
            for item in v:
                new.update(transform_dict(item))

        if isinstance(v, dict):
            for k2, v2 in v.items():
                if k == 'selectedStakeTag':
                    if k2 == 'label':
                        new['labelmanuel'] = str(v2)
    return new


def format(filepath):
    # path should be like : 'C:/Users/name/Desktop/Python/data.json' or relative path
    with open(filepath, 'r', encoding="utf-8") as file:
        s = 0
        data = json.load(file)
        df = pd.DataFrame(transform_dict(data[0]), index=[0])
        df['nbvotes'] = df['agree'] + \
            df['neutral'] + df['disagree']
        s = df['nbvotes'][0]
        for k in range(1, len(data)):
            ligne = pd.DataFrame(transform_dict(data[k]), index=[k])
            ligne['nbvotes'] = ligne['agree'] + \
                ligne['neutral'] + ligne['disagree']

            if ligne['nbvotes'][k] >= s:
                s = ligne['nbvotes'][k]

            df = pd.concat(
                [df, ligne])
        print(s)
        # calcul d'opacité
        df['opacite'] = round(0.5 + 0.5*(df['nbvotes']/s)**(1/2), 2)
        df['labelmanuel'].fillna('Sans theme', inplace=True)
        return (df)


format('C:/Users/Jules/Desktop/Python/Pstat2A/data.json')
# .to_csv(
#    path_or_buf='C:/Users/jules/Desktop/PSTATFINAL/topic-modeling-for-make-org/data/dftest.csv', sep='|')
