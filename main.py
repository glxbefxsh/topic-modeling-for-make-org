import streamlit as st
import time
import json
import plotly.express as px
import pandas as pd


st.set_page_config(
    layout="wide", page_title="Topic Modeling for Make.org", page_icon="📊")
hide_st_style = """
            <style>
            footer {visibility: hidden;}
            thead tr th:first-child {display:none}
            tbody th {display:none}
            </style>
            """
st.markdown(hide_st_style, unsafe_allow_html=True)
placeholder = st.empty()
if 'ran' not in st.session_state:
    placeholder.write(
        "## __Topic Modeling for Make.org__\n_To begin, please configure the application using the menu on the left._")
elif not st.session_state['ran']:
    placeholder.write(
        '## __Topic Modeling for Make.org__\n_Click on the _Run_ button below to start._')

# 1. Upload
st.sidebar.write("## 📄 Uploading the corpus")
upload_expander = st.sidebar.expander("__⚙️ Options :__", expanded=True)
corpus = upload_expander.file_uploader(
    "Upload your corpus as a CSV file, where each line represents a proposition :", type=["csv", "txt"])
upload_expander.write(
    "If no file is uploaded, the default corpus (that can be found [here](https://gitlab.com/glxbefxsh/projet-stat/-/blob/main/data/data.txt)) will be used.")
st.sidebar.write('***')

# 2. Preprocessing
col1, col2 = st.sidebar.columns([5, 1])
col1.write("## 🧹 Preprocessing")
pre_checkbox = col2.checkbox(' ', value=True, key='preprocessing_checkbox')

preprocessing_expander = st.sidebar.expander("__⚙️ Options :__", expanded=True)
stop_words_file = preprocessing_expander.file_uploader(
    "Upload your stop words as a CSV file, where each line represents a stop word :", type=["csv", "txt"])
preprocessing_expander.write(
    "If no file is uploaded, the default stop words (that can be found [here](https://gitlab.com/glxbefxsh/projet-stat/-/blob/main/data/stop_words_french.txt)) will be used.")
st.sidebar.write('***')

# 3. Vectorization
col1, col2 = st.sidebar.columns([5, 1])
col1.write("## 🔢 Vectorization")
vec_checkbox = col2.checkbox(' ', value=True, key='vectorization_checkbox')

vectorization_expander = st.sidebar.expander("__⚙️ Options :__", expanded=True)
vec_params = {}
n_components = vectorization_expander.number_input(
    "Choose the number of components :", min_value=1, max_value=100, value=30)
use_skip_gram = vectorization_expander.checkbox(
    "Use SKIP-GRAM instead of CBOW  ?", value=False, key='use_skip_gram')
vec_params['output_words'] = vectorization_expander.checkbox(
    "Output words instead of sentences ?", value=False, key='output')
vec_params['enlever_les_outliers'] = vectorization_expander.checkbox(
    "Remove outliers (words that are in the 99th centile of presence) ?", value=False, key='enlever_les_outliers')
if not vec_params['output_words']:
    use_tf_idf = vectorization_expander.checkbox(
        "Use TF-IDF ?", value=False, key='use_tf_idf')
else:
    use_tf_idf = False
vec_params['max_features'] = vectorization_expander.number_input(
    "Choose the maximum number of features :", min_value=1, max_value=10000, value=1000, key='max_features')
vec_params['min_occurences'] = vectorization_expander.number_input(
    "Choose the minimum number of occurences :", min_value=1, max_value=100, value=5, key='min_occurences')
vec_params['ngram_range'] = vectorization_expander.radio(
    "Choose the n-gram range :", [(1, 1), (1, 2), (1, 3)], key='ngram_range')
st.sidebar.write('***')

# 4. Dimension reduction
col1, col2 = st.sidebar.columns([5, 1])
col1.write("## 📊 Dimension reduction")
dim_checkbox = col2.checkbox(' ', value=True, key='dimension_checkbox')

dimension_expander = st.sidebar.expander("__⚙️ Options :__", expanded=True)
dimension_method = dimension_expander.radio(
    "Choose a dimension reduction method :", ['PCA', 'TSNE', 'UMAP', 'CAH'])
n_dimensions = dimension_expander.number_input(
    "Choose the dimension :", min_value=1, max_value=10, value=5)
st.sidebar.write('***')

# 5. Clustering
col1, col2 = st.sidebar.columns([5, 1])
col1.write("## 📌 Clustering")
clu_checkbox = col2.checkbox(' ', value=True, key='clustering_checkbox')
clu_params = {}
url = "https://en.wikipedia.org/wiki/Cluster_analysis"
clustering_expander = st.sidebar.expander("__⚙️ Options :__", expanded=True)
clustering_method = clustering_expander.radio(
    "Choose a [clustering](%s) method :" % url, ['KMeans', 'DBSCAN', 'HDBSCAN', 'OPTICS'])
if clustering_method == 'KMeans':
    clu_params['n_clusters'] = clustering_expander.number_input(
        "Choose the minimum number of clusters :", min_value=1, max_value=100, value=2)
elif clustering_method == 'DBSCAN':
    clu_params['min_samples'] = clustering_expander.number_input(
        "Choose the minimum number of samples :", min_value=1, value=5)
    clu_params['eps'] = clustering_expander.number_input(
        "Choose the value of Epsilon :", min_value=0.0, value=3.0)
elif clustering_method == 'HDBSCAN':
    clu_params['min_cluster_size'] = clustering_expander.number_input(
        "Choose the minimum number of points in a cluster :", min_value=1, value=10)
elif clustering_method == 'OPTICS':
    clu_params['min_samples'] = clustering_expander.number_input(
        "Choose the minimum number of samples :", min_value=1, value=5)
    clu_params['max_eps'] = clustering_expander.number_input(
        "Choose the maximum value of Epsilon :", min_value=0.0, value=3.0)
st.sidebar.write('***')

# 6. Visualization
col1, col2 = st.sidebar.columns([5, 1])
col1.write("## 📊 Visualization")

visualization_expander = st.sidebar.expander("__⚙️ Options :__", expanded=True)
visualization_reduction = visualization_expander.radio(
    "Choose a dimension reduction method for the visualization :", ['PCA', 'TSNE', 'LDA', 'First two dimensions'])
show_votes = visualization_expander.checkbox(
    "Use votes as color :", value=False, key='show_votes')
show_wordclouds = visualization_expander.checkbox(
    "Show wordclouds :", value=True, key='show_wordclouds')
show_controversials = visualization_expander.checkbox(
    "Show graph of controversials in clusters :", value=True, key='show_controversials')
f_prop = visualization_expander.number_input(
    "Choose the fraction of propositions to display :", min_value=0.0, value=0.5)
st.sidebar.write('***')

# Buttons fuctions
if st.sidebar.button("Run", use_container_width=True):
    st.session_state['ran'] = True

if 'ran' in st.session_state and st.session_state['ran']:
    placeholder.empty()

    df = pd.read_csv(corpus, sep='|', encoding='utf-8')
    st.session_state['df'] = df

    if pre_checkbox:
        print('preprocessing...')
        from steps.preprocessing_step import preprocessing_step
        pre_df = preprocessing_step(st.session_state['df'].copy(
        ), stop_words=stop_words_file.read().decode('utf-8').splitlines())
        pre_df.dropna(inplace=True)
    else:
        pre_df = df
    st.session_state['pre_df'] = pre_df

    if vec_checkbox:
        print('vectorization...')
        from steps.vectorization_step import vectorization_step
        vec_df = vectorization_step(st.session_state['pre_df'].copy(
        ), n_components, use_skip_gram, use_tf_idf, vec_params)
        vec_df.dropna(inplace=True)
    else:
        vec_df = pre_df
    st.session_state['vec_df'] = vec_df

    if dim_checkbox:
        print('dimension...')
        from steps.dim_reduction_step import dim_reduction_step
        dim_df = dim_reduction_step(st.session_state['vec_df'].copy(
        ), method=dimension_method, n_components=n_dimensions)
        dim_df.dropna(inplace=True)
    else:
        dim_df = vec_df
    st.session_state['dim_df'] = dim_df

    if clu_checkbox:
        print('clustering...')
        from steps.clustering_step import clustering_step
        clu_df = clustering_step(st.session_state['dim_df'].copy(
        ), method=clustering_method, clu_params=clu_params)
        clu_df.dropna(inplace=True)
    else:
        clu_df = dim_df
    st.session_state['clu_df'] = clu_df

    st.write('## __Topic Modeling for Make.org__')
    tab1, tab2, tab3 = st.tabs(["Plot", "Dataframes", "Analysis"])
    st.session_state['finished'] = True

    print('visualization...')
    from steps.visualization_step import visualization_step
    viz_df, fig = visualization_step(st.session_state['clu_df'].copy(),
                                     method=visualization_reduction, show_votes=show_votes,
                                     show_wordclouds=show_wordclouds, f_prop=f_prop)

    viz_df.dropna(inplace=True)
    with tab1:
        st.plotly_chart(fig, use_container_width=True)

    if show_votes:
        st.write('Each point is given a level of red, green and blue (up to a value of 250) according to the votes made. The proportion of red is given by disagrees, green by agrees, and blue by neutral votes.')
        st.write(
            'Opacity is set according to the popularity of the proposition (the number of votes it has)')
    st.session_state['viz_df'] = viz_df
    st.session_state['ran'] = False

    with tab2:
        st.write("Here, you can download the dataframe at each step of the process, so that each step does not have to be run again. For example, you can do another analysis on the same corpus without having to preprocess it once more by uploading the preprocessed corpus directly into the app and unticking the checkbox next to _🧹 Preprocessing_ in the configuration sidebar.")
        col1, col2 = st.columns([5, 1])
        col1.write("### Preprocessed dataframe :")
        col2.download_button("Download", st.session_state['pre_df'].to_csv(sep='|', index=False),
                             use_container_width=True, key="preprocessed", file_name="pre_df.csv")
        st.dataframe(st.session_state['pre_df'], use_container_width=True)

        col1, col2 = st.columns([5, 1])
        col1.write("\n### Vectorization dataframe :")
        col2.download_button("Download", st.session_state['vec_df'].to_csv(sep='|', index=False),
                             use_container_width=True, key="vectorization", file_name="vec_df.csv")
        st.dataframe(st.session_state['vec_df'], use_container_width=True)

        col1, col2 = st.columns([5, 1])
        col1.write("### Dimension reduction dataframe :")
        col2.download_button("Download", st.session_state['dim_df'].to_csv(sep='|', index=False),
                             use_container_width=True, key="dimension", file_name="dim_df.csv")
        st.dataframe(st.session_state['dim_df'], use_container_width=True)

        col1, col2 = st.columns([5, 1])
        col1.write("### Clustering dataframe :")
        col2.download_button("Download", st.session_state['clu_df'].to_csv(sep='|', index=False),
                             use_container_width=True, key="clustering", file_name="clu_df.csv")
        st.dataframe(st.session_state['clu_df'], use_container_width=True)

        col1, col2 = st.columns([5, 1])
        col1.write("### Visualization dataframe :")
        col2.download_button("Download", st.session_state['viz_df'].to_csv(sep='|', index=False),
                             use_container_width=True, key="visualization", file_name="viz_df.csv")
        st.dataframe(st.session_state['viz_df'], use_container_width=True)

    with tab3:
        st.write("### Tick options in the menu to show results here :")

        if show_controversials:
            from analysis.controversial import controversial_plot, conditions
            fig3 = controversial_plot(st.session_state['viz_df'])
            st.write("### Controversial Plot :")
            st.plotly_chart(fig3, use_container_width=True)
            st.write("Number of controversial propositions per cluster, the definition of controversial can be changed in the code, controversial.py module in the analysis folder")

        if show_wordclouds:
            from analysis.cluster_words import cluster_words
            fig2, axes = cluster_words(st.session_state['viz_df'])
            st.write("### Word Clouds :")
            st.pyplot(fig2)
            st.write("Highest scoring words for each cluster, with the score being the number of occurences of the word in the cluster squared divided by the number of occurences in the whole corpus.")

        from analysis.cluster_details import cluster_details
        st.write("### Cluster Details :")
        st.write(cluster_details(st.session_state['viz_df']))
