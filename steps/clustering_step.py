def clustering_step(df, method, clu_params):

    # Creating a dataframe with just the components:
    columns_to_drop = []
    for column in df.columns:
        if not column.startswith('component_'):
            columns_to_drop.append(column)
    components_df = df.drop(columns=columns_to_drop, axis=1)

    # KMeans clustering
    if method == 'KMeans':
        from sklearn.cluster import KMeans
        n_clusters = clu_params['n_clusters']
        kmeans = KMeans(n_clusters=n_clusters, n_init='auto')
        kmeans.fit(components_df)
        df['clustering'] = kmeans.labels_
        final_df = df

    # DBSCAN clustering
    elif method == 'DBSCAN':
        from sklearn.cluster import DBSCAN
        eps = clu_params['eps']
        min_samples = clu_params['min_samples']
        dbscan = DBSCAN(eps=eps, min_samples=min_samples)
        dbscan.fit(components_df)
        maxi = max(dbscan.labels_)
        for val in dbscan.labels_:
            # write each item on a new line
            if val == -1:
                val = maxi + 2
        df['clustering'] = dbscan.labels_
        with open('C:/Users/jules/Desktop/PSTATFINAL/topic-modeling-for-make-org/testdbscan.py', 'w', encoding="utf-8") as file:
            for item in dbscan.labels_:
                # write each item on a new line
                file.write("%s\n" % item)

        final_df = df

    # HDBSCAN clustering
    elif method == 'HDBSCAN':
        from hdbscan import HDBSCAN
        hdbscan = HDBSCAN(algorithm='best', alpha=1.0, approx_min_span_tree=True,
                          gen_min_span_tree=False, leaf_size=40,
                          metric='euclidean', min_cluster_size=clu_params['min_cluster_size'], min_samples=None, p=None)
        hdbscan.fit(components_df)
        L = hdbscan.labels_.tolist()
        maxi = max(L)
        for k in range(len(L)):
            # write each item on a new line
            if L[k] == -1:
                L[k] = maxi + 2
        df['clustering'] = L
        with open('C:/Users/jules/Desktop/PSTATFINAL/topic-modeling-for-make-org/testHdbscan.py', 'w', encoding="utf-8") as file:
            for item in L:
                # write each item on a new line
                file.write("%s\n" % item)
        final_df = df

    # OPTICS clustering
    elif method == 'OPTICS':
        from sklearn.cluster import OPTICS
        max_eps = clu_params['max_eps']
        min_samples = clu_params['min_samples']
        optics = OPTICS(max_eps=max_eps, min_samples=min_samples)
        optics.fit(components_df)
        df['clustering'] = optics.labels_
        final_df = df

    df['clustering'] = 'cluster_' + df['clustering'].astype(str)
    return final_df
