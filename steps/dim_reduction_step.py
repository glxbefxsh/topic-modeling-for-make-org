def dim_reduction_step(df, method, n_components=2):
    import pandas as pd

    # Creating a dataframe with just the components:
    columns_to_drop = []
    for column in df.columns:
        if not column.startswith('component_'):
            columns_to_drop.append(column)
    components_df = df.drop(columns=columns_to_drop, axis=1)

    # Creating a dataframe with everything except the components:
    columns_to_drop = []
    for column in df.columns:
        if column.startswith('component_'):
            columns_to_drop.append(column)
    empty_df = df.drop(columns=columns_to_drop, axis=1)

    # PCA dimension reduction
    if method == 'PCA':
        from sklearn.decomposition import PCA
        pca = PCA(n_components)
        new_df = pd.DataFrame(pca.fit_transform(components_df), columns=[
                              'component_' + str(i) for i in range(n_components)])

    # TSNE dimension reduction
    elif method == 'TSNE':
        from sklearn.manifold import TSNE
        tsne = TSNE(n_components=n_components,
                    init='pca', learning_rate='auto', method='exact')
        new_df = pd.DataFrame(tsne.fit_transform(components_df), columns=[
                              'component_' + str(i) for i in range(n_components)])

    # UMAP dimension reduction
    elif method == 'UMAP':
        from umap import UMAP
        umap_model = UMAP(n_components=n_components)
        umap_array = umap_model.fit_transform(components_df)
        new_df = {}
        for i in range(n_components):
            new_df['component_' + str(i)] = umap_array[:, i]
        new_df = pd.DataFrame(new_df)

    else:
        print(f"unspopported method: '{method}'")
        return None

    for column in new_df.columns:
        empty_df[column] = new_df[column]
    return empty_df
