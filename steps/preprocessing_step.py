def preprocessing_step(df, stop_words, spacy_package='fr_core_news_md'):

    # Lemmatizing function
    def lemmatize(df, spacy_package):

        # Importing spacy package
        import spacy
        if not spacy.util.is_package(spacy_package):
            print("Downloading " + spacy_package + " ...")
            spacy.cli.download(spacy_package)
        nlp = spacy.load(spacy_package)

        # Text lemmatizing function
        def lemmatize_text(text):
            doc = nlp(text)
            return ' '.join([token.lemma_ for token in doc])

        # Applying the lemmatizing function to the content column
        df['lemmatized'] = df['content'].apply(lemmatize_text)
        return df

    # Stop words removing function
    def remove_stop_words(df, stop_words):

        # Text stop words removing function
        def remove_stop_words(text, stop_words):
            return ' '.join([word for word in text.split() if word not in stop_words])

        # Applying the stop words removing function to the content column
        df['lemmatized'] = df['lemmatized'].apply(
            remove_stop_words, args=(stop_words,))
        return df

    # Applying the lemmatizing and stop words removing functions
    df = lemmatize(df, spacy_package)
    df = remove_stop_words(df, stop_words)
    return df
