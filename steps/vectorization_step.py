def vectorization_step(df, n_components, use_skip_gram, use_tf_idf, vec_params={}):

    # Function that applies the word2vec model to a sentence
    import numpy as np
    import pandas as pd

    og_df = df.copy()
    max_features = vec_params['max_features']
    min_df = vec_params['min_occurences']
    ngram_range = vec_params['ngram_range']
    my_sentences = [sentence.split(' ')
                    for sentence in df['lemmatized'].values.tolist()]

    if use_tf_idf:
        import numpy as np
        from sklearn.feature_extraction.text import TfidfVectorizer

        use_idf = True
        analyzer = 'word'
        stop_words = None

        # CALCUL DU TFIDF
        vectorizer = TfidfVectorizer(input='content',
                                     encoding='utf-8',
                                     max_features=max_features,
                                     min_df=min_df / df.shape[0],
                                     max_df=100 / df.shape[0],
                                     use_idf=use_idf,
                                     ngram_range=ngram_range,
                                     analyzer=analyzer,
                                     stop_words=stop_words)

        tfidf_matrix = vectorizer.fit_transform(df['lemmatized'])
        word_to_weight = {}
        features = vectorizer.get_feature_names_out()
        for i in range(len(features)):
            word_to_weight[features[i]] = tfidf_matrix[:, i]
        word_occurences = {word: np.sum(
            tfidf_matrix[:, i].toarray()) for i, word in enumerate(features)}

        for sentence in my_sentences:
            for word in sentence:
                if word not in word_to_weight:
                    word_to_weight[word] = np.zeros((tfidf_matrix.shape[0], 1))

    # Training the model
    from gensim.models import Word2Vec
    model = Word2Vec(
        sentences=my_sentences, vector_size=n_components, min_count=min_df, sg=int(use_skip_gram), max_final_vocab=max_features,
        window=10, negative=10)
    model.train(df['lemmatized'].values,
                total_examples=df.size, epochs=15)

    # Just the words
    if vec_params['output_words']:
        content = []
        components = {f'component_{component}': []
                      for component in range(n_components)}
        for word in model.wv.index_to_key:
            content.append(word)
            for component in range(n_components):
                components[f'component_{component}'].append(
                    model.wv[word][component])
        df = pd.DataFrame(components)
        df['lemmatized'] = content

    # Sentences
    else:
        final_df_dic = {'lemmatized': []}
        for component in range(n_components):
            final_df_dic[f'component_{component}'] = []
        for i in range(5):
            sentence = my_sentences[i]
            # print(sentence, [(word, word_to_weight[word][i, 0])
            #                  for word in sentence if word in model.wv.index_to_key])
        for i in range(len(my_sentences)):
            sentence = my_sentences[i]
            if use_tf_idf:
                sentence2list = [word_to_weight[word][i, 0] * model.wv[word]
                                 for word in sentence if word in model.wv.index_to_key]
            else:
                sentence2list = [model.wv[word]
                                 for word in sentence if word in model.wv.index_to_key]
            if len(sentence2list) != 0:
                sentence2vec = np.mean(np.array(sentence2list), axis=0)
            else:
                sentence2vec = np.nan * np.ones(n_components)
            # sentence2vec /= np.linalg.norm(sentence2vec)
            final_df_dic['lemmatized'].append(' '.join(sentence))
            for component in range(n_components):
                final_df_dic[f'component_{component}'].append(
                    sentence2vec[component])
        df = pd.DataFrame(final_df_dic)

    for column in og_df.columns:
        if not column.startswith('component'):
            df[column] = og_df[column]

    # kick outliers
    if vec_params['enlever_les_outliers']:
        dic = {}
        for sentence in my_sentences:
            for word in sentence:
                if word not in dic.keys():
                    dic[word] = 0
                dic[word] += 1
        # with open('C:/Users/Jules/Desktop/test.txt', mode='w') as file:
        #     for key, value in dic.items():
        #         file.write(f'{key}, {value}, \n')
        for key, value in dic.items():
            print('test')
    return df
