def visualization_step(df, method, show_votes, show_wordclouds, f_prop):
    import pandas as pd
    if f_prop > 1.00:
        f_prop = 1.00

    # Creating a dataframe with just the components:
    columns_to_drop = []
    for column in df.columns:
        if not column.startswith('component_'):
            columns_to_drop.append(column)
    components_df = df.drop(columns=columns_to_drop, axis=1)

    # Creating a dataframe with everything except the components:
    columns_to_drop = []
    for column in df.columns:
        if column.startswith('component_'):
            columns_to_drop.append(column)
    empty_df = df.drop(columns=columns_to_drop, axis=1)

    # PCA dimension redction
    if method == 'PCA':
        from sklearn.decomposition import PCA
        pca = PCA(n_components=2)
        new_df = pd.DataFrame(pca.fit_transform(components_df), columns=[
            'component_' + str(i) for i in range(2)])

    # t-SNE dimension reduction
    elif method == 'TSNE':
        from sklearn.manifold import TSNE
        tsne = TSNE(n_components=2)
        new_df = pd.DataFrame(tsne.fit_transform(components_df), columns=[
            'component_' + str(i) for i in range(2)])

    # LDA dimension reduction
    elif method == 'LDA':
        from sklearn.discriminant_analysis import LinearDiscriminantAnalysis
        clf = LinearDiscriminantAnalysis(n_components=2)
        X = components_df.values
        Y = df['clustering'].values
        X_new = clf.fit_transform(X, Y)
        new_df = pd.DataFrame(
            X_new, columns=['component_' + str(i) for i in range(2)])

    # First two components
    elif method == 'First two dimensions':
        new_df = components_df[['component_0', 'component_1']]

    for column in new_df.columns:
        empty_df[column] = new_df[column]
    final_df = empty_df

    # Plotting modules
    import plotly.express as px
    import pandas as pd
    from plotly.validators.scatter.marker import SymbolValidator

    # Plotting
    full_df = final_df.copy()
    final_df = final_df.sample(frac=f_prop)

    if show_votes:
        colorlist = []
        la, ld, ln = final_df['agreergb'].values.tolist(
        ), final_df['disagreergb'].values.tolist(), final_df['neutralrgb'].values.tolist()
        for k in range(len(la)):
            r, g, b = ld[k], la[k], ln[k]
            colorlist.append(f'rgb({r},{g},{b})')
        final_df['cluster_nb'] = final_df['clustering'].str.split(
            '_').str[1].astype(int)
        fig = px.scatter(final_df, x='component_0', y='component_1', template="simple_white",
                         height=600, hover_name='content', symbol=final_df['cluster_nb'].values.tolist(), symbol_map='identity',
                         hover_data={
                             'component_0': False, 'component_1': False, 'clustering': None, 'agreergb': True, 'neutralrgb': True,
                             'disagreergb': True, 'opacite': True},
                         color=colorlist, color_discrete_map="identity", opacity=final_df['opacite'].values.tolist())

    else:
        fig = px.scatter(final_df, x='component_0', y='component_1', template="simple_white",
                         height=600, hover_name='content', color='clustering',
                         hover_data={
                             'component_0': False, 'component_1': False, 'clustering': None})

    fig.update_layout(yaxis_title=None)
    fig.update_layout(xaxis_title=None)
    fig.update_layout(margin=dict(l=0, r=0, t=0, b=0))
    fig.update_xaxes(visible=False)
    fig.update_yaxes(visible=False)
    fig.update_layout(plot_bgcolor='rgba(250,250,250,1)')

    return full_df, fig
